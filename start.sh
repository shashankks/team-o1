#!/bin/bash

sudo su <<EOF
ulimit -SHn 999999
ulimit -n
export PATH=$PATH:/usr/local/go/bin
mkdir -p offset-tables key-space-tables
go run web-server/main.go -connections 300000
EOF