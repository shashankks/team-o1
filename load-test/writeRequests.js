

'use strict';

/**
 * Sample request generator usage.
 * Contributed by jjohnsonvng:
 * https://github.com/alexfernandez/loadtest/issues/86#issuecomment-211579639
 */

const loadtest = require('loadtest');
const { v4: uuidv4 } = require('uuid');
const process = require('process');

const raw_message = {
    "probeId": "PRB34222422421123",
    "eventId": "7707d6a0-61b5-11ec-9f10-0800200c9a66",
    "messageType": "spaceCartography",
    "eventTransmissionTime": Date.now(),
    "messageData": [
        {
            "measureName": "Spherical coordinate system - euclidean distance",
            "measureCode": "SCSED",
            "measureUnit": "parsecs",
            "measureValue": 5399e5,
            "measureValueDescription": "Euclidean distance from earth",
            "measureType": "Positioning",
            "componentReading": 43e23
        },
        {
            "measureName": "Spherical coordinate system - azimuth angle",
            "measureCode": "SCSEAA",
            "measureUnit": "degrees",
            "measureValue": 170.42,
            "measureValueDescription": "Azimuth angle from earth",
            "measureType": "Positioning",
            "componentReading": 46e2
        },
        {
            "measureName": "Spherical coordinate system - polar angle",
            "measureCode": "SCSEPA",
            "measureUnit": "degrees",
            "measureValue": 30.23,
            "measureValueDescription": "Polar/Inclination angle from earth",
            "measureType": "Positioning",
            "componentReading": 56e42
        },
        {
            "measureName": "Localized electromagnetic frequency reading",
            "measureCode": "LER",
            "measureUnit": "hz",
            "measureValue": 3e5,
            "measureValueDescription": "Electromagnetic frequency reading",
            "measureType": "Composition",
            "componentReading": 3e15
        },
        {
            "measureName": "Probe lifespan estimate",
            "measureCode": "PLSE",
            "measureUnit": "Years",
            "measureValue": 2390e2,
            "measureValueDescription": "Number of years left in probe lifespan",
            "measureType": "Probe",
            "componentReading": 6524e3
        }
    ]
}


const multiples = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const multiplier = 500;

const concurencies = multiples.map(i => i * multiplier);
const rpss = multiples.map(i => i * multiplier);

function test(concIndex, rpsIndex) {
    const options = {
        concurrency: concurencies[concIndex],
        requestsPerSecond:rpss[rpsIndex],
        url: 'http://' + process.argv[2] + ':' + process.argv[3],
        method: 'PUT',
        body:'',
        maxSeconds:5,
        requestGenerator: (params, options, client, callback) => {
            const probe_id = uuidv4()
            // console.log("Probe_id",probe_id)
            const message = JSON.stringify(raw_message)
            options.headers['Content-Type'] = 'application/json';
            options.path = '/probe/' + probe_id + "/event/123";
            const request = client(options, callback);
            options.headers['Content-Length'] = message.length;
            request.write(message);
            return request;
        }
    };     
    
    loadtest.loadTest(options, (error, results) => {
        if (error) {
            return console.error('Got an error: %s', error);
        }
        console.log("conc %d | rps %d => %d (errors: %d, real rps: %d)", concurencies[concIndex], rpss[rpsIndex], results.percentiles['99'], results.totalErrors, results.rps);
        // console.log(results);

        if(concIndex + 1 == concurencies.length) return;
        if(rpsIndex + 1 == rpss.length) {
            test(concIndex + 1, 0);
        } else {
            test(concIndex, rpsIndex + 1)
        }
    });
}

test(0, 0);