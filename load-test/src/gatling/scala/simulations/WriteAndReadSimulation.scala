package simulations

import io.gatling.core.Predef._
import io.gatling.http.HeaderNames.{Accept, ContentType}
import io.gatling.http.HeaderValues.ApplicationJson
import io.gatling.http.Predef._

class WriteAndReadSimulation extends Simulation {
  private val host = "localhost"
  private val port = "7072"
  private val baseUrl = s"http://$host:$port"

  private val httpProtocol = http.baseUrl(baseUrl)
    .header(ContentType, ApplicationJson)
    .header(Accept, ApplicationJson)


  private val scn = scenario("POST Station Visits")
    .exec(
      http("write data")
        .put(session => f"/probe/${session.userId}/event/${session.userId * 2}")
        .body(RawFileBody("sample_payload.json")).asJson
    ).pause(3)
    .exec(
      http("get data")
        .get(session => f"/probe/${session.userId}/latest")
    )

  setUp(scn.inject(atOnceUsers(500)).protocols(httpProtocol))
}
