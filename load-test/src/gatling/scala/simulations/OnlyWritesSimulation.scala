package simulations

import io.gatling.core.Predef._
import io.gatling.http.HeaderNames.{Accept, ContentType}
import io.gatling.http.HeaderValues.ApplicationJson
import io.gatling.http.Predef._

import scala.util.Random

class OnlyWritesSimulation extends Simulation {
  private val host = "localhost"
  private val port = "7072"
  private val baseUrl = s"http://$host:$port"

  private val httpProtocol = http.baseUrl(baseUrl)
    .header(ContentType, ApplicationJson)
    .header(Accept, ApplicationJson)


  private val scn = scenario("POST Station Visits")
    .exec(_.set("transmissionTime", Math.abs(Random.nextLong).toString))
    .exec(
      http("write data")
        .put(session => f"/probe/${session.userId}/event/${session.userId * 2}")
        .body(PebbleFileBody("sample_payload.json")).asJson
        .check(status.is(204))
    )

  setUp(scn.inject(atOnceUsers(250)).protocols(httpProtocol))
}
