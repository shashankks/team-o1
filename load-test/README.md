To run the load test: 

1. First start the server with `go run web-server/main.go`
2. In another terminal, `cd load-test && ./gradlew gatlingRun`

