package main

import (
	"fmt"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestAppendEventReceivedTime(t *testing.T) {
	t.Run("should append eventReceivedTime to json", func(t *testing.T) {
		epochTime := 432434342
		payload := `{"a": 1, "b": 2 }`
		expected := fmt.Sprintf("{\"a\": 1, \"b\": 2, \"eventReceivedTime\": %v }", epochTime)

		actual := appendEventReceivedTime([]byte(payload), int64(epochTime))
		require.JSONEq(t, expected, actual)
	})
}
