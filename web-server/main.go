package main

import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"runtime/debug"
	"time"
	"unsafe"

	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp"
	//"github.com/valyala/fasthttp/pprofhandler"
	"os"
	"runtime/pprof"

	"gitlab.com/shashankks/team-o1/lfs"
)

var (
	addr        = flag.String("addr", ":7072", "TCP address to listen to")
	compress    = flag.Bool("compress", false, "Whether to enable transparent response compression")
	connections = flag.Int("connections", 16000, "Number of connections server can accept")
	cpuprofile  = flag.String("cpuprofile", "", "write cpu profile to `file`")
	database    *lfs.LFS
)

func main() {
	go periodicFree(4000 * time.Millisecond)
	flag.Parse()
	//cpuProfiling()
	database = lfs.New()
	r := router.New()
	r.GET("/probe/{probe_id}/latest", getLatestProbeValueHandler)
	r.PUT("/probe/{probe_id}/event/{event_id}", putProbeValueHandler)
	//r.GET("/debug/pprof/{profile:*}", pprofhandler.PprofHandler)

	h := r.Handler
	if *compress {
		h = fasthttp.CompressHandler(h)
	}
	s := &fasthttp.Server{
		Handler:           globalCounter(h),
		ReduceMemoryUsage: true,
	}
	err := s.ListenAndServe(*addr)
	if err != nil {
		log.Fatalf("Error in ListenAndServe: %s", err)
	}
}

func globalCounter(next fasthttp.RequestHandler) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		if lfs.ActiveConnections-int64(*connections) >= 0 {
			ctx.Response.SetStatusCode(429)
			return
		} else {
			next(ctx)
		}
	}
}

func cpuProfiling() {
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal("could not create CPU profile: ", err)
		}
		defer f.Close()
		if err := pprof.StartCPUProfile(f); err != nil {
			log.Fatal("could not start CPU profile: ", err)
		}
		log.Printf("Started cpu profiling")
		defer pprof.StopCPUProfile()
	}
}

func periodicFree(d time.Duration) {
	log.Println("Returning memory to OS")
	tick := time.Tick(d)
	for _ = range tick {
		log.Println("Currently serving requests", lfs.ActiveConnections)
		debug.FreeOSMemory()
	}
}

func putProbeValueHandler(ctx *fasthttp.RequestCtx) {
	probeId := ctx.UserValue("probe_id").(string)
	rawEvent := ctx.PostBody()
	event := appendEventReceivedTime(rawEvent, time.Now().UnixMilli())
	WriteValue(probeId, event)
	ctx.SetStatusCode(200)
}

func appendEventReceivedTime(data []byte, epochTime int64) []byte {
	ins := []byte(fmt.Sprintf(", \"eventReceivedTime\": %v", epochTime))
	closingBraceIndex := bytes.LastIndexByte(data, '}')
	data = append(data[:closingBraceIndex], ins...)
	data = append(data, '}')
	return data
}

func WriteValue(key string, value []byte) {
	eventCopy := lfs.BytePool.Get().([]byte)
	eventCopy = eventCopy[:0]
	eventCopy = append(eventCopy, value...)
	value = nil
	database.WriteToChan(key, b2s(eventCopy))
}

func getLatestProbeValueHandler(ctx *fasthttp.RequestCtx) {
	ctx.SetContentType("application/json")
	if pv := fetchProbeValue(ctx.UserValue("probe_id").(string)); pv != "" {
		fmt.Fprint(ctx, pv)
	} else {
		ctx.SetStatusCode(404)
	}
}

func fetchProbeValue(probeId string) string {
	return database.Read(probeId)
}

func b2s(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}
