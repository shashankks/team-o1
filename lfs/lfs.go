package lfs

import (
	"log"
	"reflect"
	"strings"
	"sync"
	"unsafe"
)

var threshold = 10000
var ActiveConnections int64

const PayloadSize int64 = 21480

type LFS struct {
	lookupTables map[string]*memTable
}

var BytePool = sync.Pool{
	New: func() interface{} {
		return []byte{}
	},
}

var mapPool = sync.Pool{
	New: func() interface{} {
		return make(map[string]string, threshold)
	},
}

func New() *LFS {
	l := LFS{}
	l.lookupTables = make(map[string]*memTable, 35)
	for _, identifier := range keySpace() {
		l.lookupTables[identifier] = newMemTable(identifier)
	}
	return &l
}

func (lfs *LFS) WriteToChan(key, value string) {
	keySpace := getKeySpace(key)
	if memTable, ok := lfs.lookupTables[keySpace]; ok {
		memTable.writeChan <- KV{key: key, value: value}
	}
}

func (lfs *LFS) Read(key string) string {
	if keyspace, ok := lfs.lookupTables[getKeySpace(key)]; ok {
		c := make(chan string)
		m := ReadChanMessage{Key: key, ReturnChan: c}
		keyspace.ReadChan <- m
		value := <-c
		close(c)
		return value
	}
	return ""
}

type ReadChanMessage struct {
	Key        string
	ReturnChan chan string
}

func keySpace() []string {
	var k []string
	for i := 'a'; i <= 'z'; i++ {
		k = append(k, string(i))
	}
	for i := '0'; i <= '9'; i++ {
		k = append(k, string(i))
	}
	log.Printf("Keyspace %v", k)
	return k
}
func getKeySpace(key string) string {
	if len(key) <= 3 {
		return strings.ToLower(string(key[0]))
	}
	return strings.ToLower(string(key[3]))
}

func b2s(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}

func s2b(s string) (b []byte) {
	bh := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	sh := (*reflect.StringHeader)(unsafe.Pointer(&s))
	bh.Data = sh.Data
	endOfString := unsafe.Add(unsafe.Pointer(sh.Data), len(s))
	*(*byte)(endOfString) = byte(0)
	bh.Cap = int(PayloadSize)
	bh.Len = int(PayloadSize)
	return b
}
