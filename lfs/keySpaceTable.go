package lfs

import (
	"bytes"
	"github.com/valyala/fastjson"
	"io"
	"log"
	"os"
	"reflect"
	"sync/atomic"
	"time"
	"unsafe"
)

type memTable struct {
	keySpace      string
	treeMap       map[string]string
	writeChan     chan KV
	ReadChan      chan ReadChanMessage
	fileWriteChan chan map[string]string
	offsetTable   *offsetTable
	keySpaceFile  *os.File
	timeoutChan   chan bool
}

type KV struct {
	key   string
	value string
}

func getTransmissionTime(rawText []byte) int {
	return fastjson.GetInt(rawText, "eventTransmissionTime")
}

func s2bOfPayload(s string) (b []byte) {
	bh := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	sh := (*reflect.StringHeader)(unsafe.Pointer(&s))
	bh.Data = sh.Data
	bh.Cap = sh.Len
	bh.Len = sh.Len
	return b
}
func (m *memTable) listener() {
	for {
		select {
		case kv := <-m.writeChan:
			existingRecord := m.Read(kv.key)
			currentRecordTransmissionTime := 0
			val := s2bOfPayload(kv.value)
			newRecordTransmissionTime := getTransmissionTime(val)
			b2s(val)
			if existingRecord != "" {
				currentRecordTransmissionTime = getTransmissionTime(s2bOfPayload(existingRecord))
			}
			if currentRecordTransmissionTime < newRecordTransmissionTime {
				atomic.AddInt64(&ActiveConnections, 1)
				m.treeMap[kv.key] = kv.value
				if len(m.treeMap) >= threshold {
					log.Println("Threshold reached, Writing to disk, keySpace", m.keySpace)
					m.fileWriteChan <- m.treeMap
					m.treeMap = mapPool.Get().(map[string]string)
					for i := range m.treeMap {
						delete(m.treeMap, i)
					}
				}
			}
		case readReq := <-m.ReadChan:
			key := readReq.Key
			returnChan := readReq.ReturnChan
			returnChan <- m.Read(key)
		case <-m.timeoutChan:
			if len(m.treeMap) > 0 {
				log.Println("Clock: Writing to disk, keySpace", m.keySpace)
				m.fileWriteChan <- m.treeMap
				m.treeMap = mapPool.Get().(map[string]string)
				for i := range m.treeMap {
					delete(m.treeMap, i)
				}
			}
		}
	}
}

func (m *memTable) KeySpaceTableWrite(mem map[string]string) {
	lengthOfTable := len(mem)
	if locationToWrite, err := m.keySpaceFile.Seek(0, io.SeekEnd); err != nil {
		log.Printf("Error while seeking to  endOfFile %v", err)
	} else {
		for key := range mem {
			c := make(chan int64)
			readChan := offsetTableReadChan{
				key,
				c,
			}
			m.offsetTable.readChan <- readChan
			offsetValue := <-c
			close(c)
			if offsetValue != -1 {
				m.replaceExistingValueInFile(offsetValue, mem[key])
			} else {
				m.appendToFile(key, mem[key], locationToWrite)
				locationToWrite = locationToWrite + PayloadSize
			}
			delete(mem, key)
		}
	}
	mapPool.Put(mem)
	atomic.AddInt64(&ActiveConnections, int64(-1*(lengthOfTable)))
}

func (m *memTable) replaceExistingValueInFile(offsetValue int64, value string) {
	locationToWrite := offsetValue
	buffer := s2b(value)
	_, err := m.keySpaceFile.WriteAt(buffer, locationToWrite)
	if err != nil {
		log.Printf("Error while writing key at location %v", err)
	}
	BytePool.Put(buffer)
}

func (m *memTable) appendToFile(key, value string, locationToWrite int64) {
	buffer := s2b(value)
	_, err := m.keySpaceFile.WriteAt(buffer, locationToWrite)
	if err != nil {
		log.Printf("Error while writing append buffer at location %v", err)
	} else {
		m.offsetTable.writeChan <- offset{key: key, offsetValue: locationToWrite}
	}
	BytePool.Put(buffer)
}

func (m *memTable) KeySpaceTableRead(key string) string {
	c := make(chan int64)
	readChan := offsetTableReadChan{
		key,
		c,
	}
	m.offsetTable.readChan <- readChan
	offsetValue := <-c
	close(c)
	if offsetValue != -1 {
		buffer := make([]byte, PayloadSize)
		if _, err := m.keySpaceFile.ReadAt(buffer, offsetValue); err != nil {
			log.Printf("error while reading to buffer from file %v", err)
			return ""
		} else {
			return retrievePayload(buffer)
		}
	} else {
		return ""
	}
}

func retrievePayload(buffer []byte) string {
	n := bytes.Index(buffer, []byte{0}) // Linear search :(
	return b2s(buffer[:n])
}

func newMemTable(keySpace string) *memTable {
	m := memTable{}
	m.treeMap = mapPool.Get().(map[string]string)
	m.writeChan = make(chan KV, 10000)
	m.ReadChan = make(chan ReadChanMessage)
	m.timeoutChan = make(chan bool)
	m.keySpace = keySpace
	m.fileWriteChan = make(chan map[string]string, 20)
	m.offsetTable = newOffsetTable(keySpace)
	if f, err := os.OpenFile("./key-space-tables/"+keySpace, os.O_CREATE|os.O_RDWR, 0644); err != nil {
		log.Fatalf("Error while creating log file for keyspace %v", err)
	} else {
		m.keySpaceFile = f
	}
	go m.timeOutProducer()
	go m.listener()
	go m.fileWriteListener()
	log.Printf("memtable for %v created", keySpace)
	return &m
}

func (m *memTable) Read(key string) string {
	if value, ok := m.treeMap[key]; ok {
		return value
	}
	return m.ReadFromDisk(key)
}

func (m *memTable) ReadFromDisk(key string) string {
	return m.KeySpaceTableRead(key)
}

func (m *memTable) timeOutProducer() {
	time.Sleep(5 * time.Second)
	for {
		time.Sleep(5 * time.Second)
		m.timeoutChan <- true
	}
}

func (m *memTable) fileWriteListener() {
	for {
		select {
		case mem := <-m.fileWriteChan:
			m.KeySpaceTableWrite(mem)
		}
	}
}
