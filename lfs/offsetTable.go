package lfs

import (
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
)

type offsetTable struct {
	keySpace   string
	writeChan  chan offset
	readChan   chan offsetTableReadChan
	table      map[string]int64
	offsetFile *os.File
}

type offset struct {
	key         string
	offsetValue int64
}
type offsetTableReadChan struct {
	key        string
	returnChan chan int64
}

func (o *offsetTable) listener() {
	for {
		select {
		case offset := <-o.writeChan:
			_, err := o.offsetFile.Write([]byte(offset.key + "\t" + strconv.FormatInt(offset.offsetValue, 10) + "\t"))
			if err != nil {
				log.Printf("Could not write offset value to file %v", err)
			} else {
				o.table[offset.key] = offset.offsetValue
			}
		case readReq := <-o.readChan:
			key := readReq.key
			returnChan := readReq.returnChan
			if value, ok := o.table[key]; ok {
				returnChan <- value
			} else {
				returnChan <- -1
			}
		}
	}
}

func newOffsetTable(keySpace string) *offsetTable {
	var table = make(map[string]int64, 2000)
	f, err := os.OpenFile("./offset-tables/"+keySpace, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		log.Fatalf("Error while creating log file for keyspace %v", err)
	}
	o := offsetTable{
		keySpace:   keySpace,
		writeChan:  make(chan offset, 500),
		readChan:   make(chan offsetTableReadChan),
		table:      table,
		offsetFile: f,
	}
	o.initialiseOffsetTableFromFile()
	go o.listener()
	return &o
}

func (o *offsetTable) initialiseOffsetTableFromFile() {
	if contents, err := ioutil.ReadAll(o.offsetFile); err != nil {
		log.Printf("error while reading offset table from file %v", err)
	} else {
		keyValue := strings.Split(string(contents), "\t")
		log.Println("Length of stored offset table", len(keyValue))
		for i := 0; i < len(keyValue)-1; i = i + 2 {
			if offsetValue, err := strconv.Atoi(keyValue[i+1]); err != nil {
				log.Printf("Could not convert to int from string %v", err)
			} else {
				o.table[keyValue[i]] = int64(offsetValue)
			}
		}
	}
}
